<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <title>${ID}</title>

    <link href="static/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <link href="static/css/style.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="static/js/html5shiv.min.js"></script>
    <script src="static/js/respond.min.js"></script>
    <![endif]-->

    <script src="static/js/jquery.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
    <script src="static/js/bootstrap-datepicker.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="static/js/ie10-viewport-bug-workaround.js"></script>
    <script src="static/js/script.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">${B}</a>
        </div>
    </div>
    <!--/.container-fluid -->
</nav>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Student info:</h3>
        </div>
        <ul class="list-group">
            <li class="list-group-item list-group-item-lg">ID: ${ID}</li>
            <li class="list-group-item list-group-item-lg">Name: ${B}</li>
            <li class="list-group-item list-group-item-lg">Info: ${C}</li>
            <li class="list-group-item list-group-item-lg">Info2: ${D}</li>
        </ul>
    </div>
</div>
</body>
</html>
