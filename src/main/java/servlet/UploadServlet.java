package servlet;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.SpreadsheetMLPackage;
import org.docx4j.openpackaging.parts.SpreadsheetML.WorksheetPart;
import org.xlsx4j.exceptions.Xlsx4jException;
import org.xlsx4j.sml.Cell;
import org.xlsx4j.sml.Row;
import org.xlsx4j.sml.SheetData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tim on 08.12.2015.
 */
public class UploadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter wr = resp.getWriter();
        wr.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <title>File Upload</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<form method=\"POST\" enctype=\"multipart/form-data\" >\n" +
                "    File:\n" +
                "    <input type=\"file\" name=\"file\" id=\"file\" /> <br/>\n" +
                "    <input type=\"submit\" value=\"Upload\" name=\"upload\" id=\"upload\" />\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>");
        wr.flush();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!ServletFileUpload.isMultipartContent(req)) {
            throw new ServletException("file missing");
        }
        try {
            resp.setContentType("text/plain; charset=UTF-8");
            resp.setCharacterEncoding("UTF-8");

            ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
            List<FileItem> items = (List<FileItem>) upload.parseRequest(req);
            for (FileItem item : items) {
                if(item.getName()==null)
                    continue;
                InputStream is = item.getInputStream();
                File f = File.createTempFile("students", ".tmp");
                FileOutputStream os = new FileOutputStream(f);

                byte[] buf = new byte[1024];
                int size;
                while((size=is.read(buf))!=-1){
                    os.write(buf, 0, size);
                }
                os.flush();
                os.close();
                processFile(f.getAbsolutePath());
            }
        } catch (FileUploadException e) {
            throw new ServletException(e);
        }
        PrintWriter writer = resp.getWriter();
        writer.println("Upload complete.");
    }

    private void processFile(String path){
        try {
            SpreadsheetMLPackage pkg = SpreadsheetMLPackage.load(new File(path));
            WorksheetPart sheet = pkg.getWorkbookPart().getWorksheet(0);
            SheetData sheetData = sheet.getContents().getSheetData();
            List<Row> rows =sheetData.getRow();
            int rowIdx = 1;
            StudentsServlet.getData().clear();
            for(Row row: rows){
                List<Cell> cells = row.getC();
                String id = "";
                HashMap<String, String> map = new HashMap<String, String>();
                for(Cell cell: cells){
                    String col = cell.getR().replace(String.valueOf(rowIdx), "");
                    if(col.equals("A")){
                        id = cell.getV();
                    } else {
                        map.put(col, cell.getV());
                    }
//                    System.out.print(cell.getV() + " ");
                }
                StudentsServlet.getData().put(id, map);
//                System.out.println();
                rowIdx++;
            }
        } catch (Docx4JException e) {
            e.printStackTrace();
        } catch (Xlsx4jException e) {
            e.printStackTrace();
        }
    }

}
