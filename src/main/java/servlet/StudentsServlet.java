package servlet;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tim on 08.12.2015.
 */
public class StudentsServlet extends HttpServlet {

    private static HashMap<String, HashMap<String, String>> data = new HashMap<String, HashMap<String, String>>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();
        String[] arr = uri.split("/");
        String id = "";
        if(arr.length>0){
            id = arr[arr.length-1];
        }
        HashMap<String, String> map = getData().get(id);
        HashMap<String, String> values = new HashMap<String, String>();
        values.put("ID", id);
        if(map!=null){
            for(String key: map.keySet()){
                values.put(key, map.get(key));
            }
        }

        Configuration config = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        config.setTemplateExceptionHandler(TemplateExceptionHandler.DEBUG_HANDLER.IGNORE_HANDLER);
        Template template = null;
        try {
            template = new Template("t1", new FileReader(getServletContext().getRealPath("WEB-INF")+"/views/student.ftl"), config);
            StringWriter sw = new StringWriter();
            template.process(values, sw);
            resp.getWriter().println(sw.toString());
            resp.getWriter().flush();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    public static HashMap<String, HashMap<String, String>> getData() {
        return data;
    }
}
